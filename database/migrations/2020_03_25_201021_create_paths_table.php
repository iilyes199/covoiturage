<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePathsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paths', function (Blueprint $table) {
            $table->bigIncrements('id');
           
            $table->string('departure_governorate');         
            $table->string('arrival_governorate');
            $table->string('departure_delegation');         
            $table->string('arrival_delegation');
            $table->dateTime('departure_date');        
            $table->integer('numplaces');
            $table->integer('numplacesavailable');
            $table->double('price');
            $table->string('cartype');
            $table->string('preference');
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paths');
    }
}
