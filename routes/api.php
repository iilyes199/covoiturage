<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::get('users', 'AuthController@index');
    Route::get('user/{id}', 'AuthController@user');
    Route::get('usertype/{type}', 'AuthController@usertype');
    Route::get('userdate/{startdate}/{enddate}', 'AuthController@userdate');
    Route::get('userdeleted/{type}', 'AuthController@userdeleted');
    Route::get('userbyemail/{email}', 'AuthController@userbyemail');
    
    Route::post('update/{id}', 'AuthController@update');
    Route::delete('delete/{id}', 'AuthController@delete');
});

Route::middleware('jwt.auth')->group( function() {
   // Route::resource('location','API\LocationController');
    
    Route::post('location','API\LocationController@store');
    Route::get('location/users/{id}','API\LocationController@usersloc');
});
//gouvernorate 
Route::middleware('jwt.auth')->group( function($router) {
    Route::post('governorate/create','API\GovernorateController@create');
    Route::get('governorates','API\GovernorateController@index');
    Route::post('delegation/create','API\DelegationController@create');
    Route::get('delegations/{id}','API\DelegationController@index');
    
});
//path
Route::middleware('jwt.auth')->group( function($router) {
    Route::post('path/create','API\PathController@create');
    Route::get('pathdriver/{govd}/{govar}/{datdep}','API\PathController@driverpath');
    Route::get('getpaths/{govd}/{govar}/{datdep}','API\PathController@getpaths');
    Route::get('pathdetails/{id}','API\PathController@pathdetails');
    Route::get('getdetailedpaths/{dd}/{da}/{dated}/{gn}','API\PathController@getdetailedpaths');


  
});

//profile
Route::middleware('jwt.auth')->group( function($router) {
    Route::put('profile/update','API\ProfileController@update');
    Route::get('profile','API\ProfileController@get');
  
});



