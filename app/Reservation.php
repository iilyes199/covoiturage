<?php

namespace App;
use App\User;
use App\Path;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'user_id','path_id','state'
     ];


     public function path() {

        return $this->belongsTo('App\Path');
  
    }

     public function user() {

        return $this->belongsTo('App\User');
  
    }

}
