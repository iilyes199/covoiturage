<?php

namespace App;
use App\Path;
use Illuminate\Database\Eloquent\Model;

class Pathdetail extends Model
{
    



    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'path_id', 'departure_delegation', 'arrival_delegation','cartype',
        'preference'
    ];



    public function path() {

        return  $this->belongsTo('App\Path');
    
    }
}
