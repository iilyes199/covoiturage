<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseController as BaseController;

class AuthController extends BaseController
{

    public function user($id){

        $user = User::find($id);
        
       
        return $this->sendResponse($user->toArray(),'User readed succesfully');
    }
/*Delete user*/ 
public function delete($id){

    $user = User::find($id);
    $user->delete();

    return $this->sendResponse($user->toArray(),'user deleted succesfully');

}

/*find user by email */
public function userbyemail($email){

$user = User::where('email', $email)->get();

if (is_null($user)) {
    return $this->sendError('User Not found');
}
return $this->sendResponse($user,'User with email readed succesfully');
}

/*find deleted users*/
public function userdeleted($type){

    $users = User::where('type', $type)->onlyTrashed()->get();

    if (is_null($users)) {
        return $this->sendError('Users Not found');
    }
    return $this->sendResponse($users,'Users deleting readed succesfully');

}

/*find user by date*/

public function userdate(Request $request,$startdate,$enddate){ 
  
      $users = User::whereBetween('created_at', [$startdate,$enddate])->get();

      if (is_null($users)) {
        return $this->sendError('User Not found');
    }
    return $this->sendResponse($users,'Users readed succesfully By Date');
   

}
/*find user by type*/ 
    public function usertype($type){
        
        $users = User::where('type', $type)->get();
              
            if (is_null($users)) {
                return $this->sendError('User Not found');
            }
            return $this->sendResponse($users,'Users readed succesfully');
    }

/*get all users*/
    public function index(){
        $users = User::all()->where('id','<>',Auth::user()->id);
        return $this->sendResponse($users->toArray(),'users readed succesfully');
        }
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Email or Password does\'t exist'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function register(Request $request){
        $input = $request->all();
        $validator = Validator::make($input,[
  'name' => 'required',
  'email' => 'required|email',
  'password' => 'required|min:8|confirmed',
  'phonenum' => 'required|min:8',
  'gender' =>'required',
  'type' => 'required'
        ]);
        if ($validator -> fails()) {
            return $this->sendError('error validation',$validator->errors());
        }
        $user = User::create([
         'name' => request('name'),
         'email' => request('email'),
         'password' => Hash::make(request('password')),
         'phonenum' => request('phonenum'),
         'gender' => request('gender'),
         'type' => request('type')
     ]);
      $profile = Profile::create([
        'user_id' => $user->id,
        'avatar' => 'avatars/1.jpg'
      ]);
     
        //$path = $request->file('avatar')->store('avatars'); 
        //$user->update(['avatar'=>$path]);
     
    
     return $this->login($user);

    }


//update function



public function update(Request $request,$id){
    $user = User::find($id);
    $validator = Validator::make($request->all(), [
        
        'avatar' => 'file',
    ]);

    if ($validator->fails()) {
        return response()->json(['error' => $validator->errors()], 401);
    }
        
    $input = $request->all();
   
 $user->name=$input['name'];
 $user->email=$input['email'];
 $user->password=Hash::make($input['password']);

 $user->avatar = $request->file('avatar')->store('avatars'); 
 
$user->save();

 return $this->sendResponse($user->toArray(),'user updated seccesfully');

}




    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' =>auth()->user()->type,
            'name' =>auth()->user()->name,
            'avatar' =>auth()->user()->avatar
        ]);
    }
}
