<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Path;
use App\User;
use App\Governorate;
use App\Delegation;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class DelegationController extends BaseController
{


    public function index($id){

        $delegation = Delegation::where('gouvname',$id)->get();
        
        return $this->sendResponse($delegation->toArray(),'Delegations readed succesfully');
 }

    public function create(Request $request){
        $input = $request->all();
      
        $validator = Validator::make($input,[
            'governorate_id' => 'required',
            'gouvname' => 'required',
             'name' => 'required',
  
        ]);
        if ($validator -> fails()) {
            return $this->sendError('error validation',$validator->errors());
        }


        
      $delegation = Delegation::create([
        'governorate_id' => request('governorate_id'),
        'gouvname' => request('gouvname'),
        'name' => request('name'),
    
    ]);

    return $this->sendResponse($delegation->toArray(),'Delegation  created succesfully');
       



    }

}