<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Location;
use App\User;
use App\Path;
use Illuminate\Support\Facades\BD;
use Validator;
use Illuminate\Support\Facades\Auth;

class LocationController extends BaseController
{

////////////////////////////////
public function usersloc($id){
   
   $user=Location::where('user_id',$id)->latest()->first();


   return $this->sendResponse($user,'location of user on parametere readed succesfully');

}
//show users location
public function index(){
$locations = Location::where('user_id',Auth::user()->id)->get();
return $this->sendResponse($locations->toArray(),'Locations readed succesfully');
}

//Register loaction
public function store(Request $request){

      $input = $request->all();
     $validator = Validator::make($input,[
'lat' => 'required',
'lng' => 'required',
'city' => 'required'
      ]);
      if ($validator -> fails()) {
          return $this->sendError('error validation',$validator->errors());
      }
   
    
 if(Location::where('user_id',Auth::user()->location)->get()){
   $Locations = Location::create([
    'user_id' => auth()->id(),
    'lat' => request('lat'),
    'lng' => request('lng'),
    'city' => request('city')
   ]);}
   else{
      $Locations = Location::where('user_id',Auth::user()->id)
      ->update(['lat'=>request('lat')],['lng'=>request('lng')],['city'=>request('city')]);
   }
   
   return $this->sendResponse($Locations,'Location created succesfully');
    
}


//find location
public function show($id){

      $location = Location::find($id);
    
      if (is_null($location)) {
          return $this->sendError('Loaction Not found');
      }

   return $this->sendResponse($location->toArray(),'Location find succesfully');
    
}

//destroy
public function destroy(Location $location){

      $location->delete();

   return $this->sendResponse($location->toArray(),'Location deleted succesfully');
    
}






}