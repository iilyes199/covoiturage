<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;


class BaseController extends Controller
{
    public function sendResponse($result,$message){
$response = [
'sucsess' => true,
'data' => $result,
'message' =>$message
];

return response()->json($response,200);

    }

    public function sendr($r,$rr,$rrr,$message){
        $response = [
            'sucsess' => true,
            'path' => $r,
            'user' => $rr,
            'location'=>$rrr,
            'message' =>$message
            ];

            return response()->json($response,200);
    }

    public function sendResponse2($r,$rr,$message){
        $response = [
            'sucsess' => true,
            'path' => $r,    
            'user' => $rr,    
            'message' =>$message
            ];

            return response()->json($response,200);
    }

    public function sendResponcePath($r,$rr,$message){
        $response = [
            'sucsess' => true,
            'path' => $r,
            'pathdetail' => $rr,      
            'message' =>$message
            ];

            return response()->json($response,200);
    }


    public function  sendError($error,$errorMessages=[],$code=404){
        $response = [
        'sucsess' => false,
        'message' =>$error
        ];
        
if(!empty($errorMessages)){
    $response['data'] = $errorMessages;
}


        return response()->json($response,$code);
        
            }



}