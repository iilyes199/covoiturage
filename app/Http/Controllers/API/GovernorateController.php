<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Path;
use App\User;
use App\Governorate;
use App\Delegation;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class GovernorateController extends BaseController
{

    public function index(){

           $governorate = Governorate::all();
           return $this->sendResponse($governorate->toArray(),'Governorate readed succesfully');
    }


    public function create(Request $request){
        $input = $request->all();
      
        $validator = Validator::make($input,[
     
    'name' => 'required',
  
        ]);
        if ($validator -> fails()) {
            return $this->sendError('error validation',$validator->errors());
        }


        
      $governorate = Governorate::create([
      
      'name' => request('name'),
    
    ]);

    return $this->sendResponse($governorate->toArray(),'Governorate created succesfully');
       



    }

}