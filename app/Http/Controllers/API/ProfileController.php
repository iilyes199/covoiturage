<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Location;
use App\User;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ProfileController extends BaseController
{
      
    public function get()
    {
          $user = Auth::user();

          return $this->sendResponse($user->toArray(),'profile readed');
    }


    public function update(Request $request)
    {

        $input = $request->all();
        $validator = Validator::make($input,[
   'name' => 'required',
   'email' => 'required|email',
         ]);
         if ($validator -> fails()) {
             return $this->sendError('error validation',$validator->errors());
         }

         $user= Auth::user();

         if( $request->hasFile('avatar')){
             $avatar=$request->avatar;
             $avatar_new_name=time().$avatar->getClientOriginalName();
             $avatar->move('avatars/',$avatar_new_name);
             $user->profile->avatar = 'avatars/'.$avatar_new_name;
             $user->profile-save();
         }
         $input = $request->all();
         $user->name=$request->name;
         $user->email=$request->email;
         $user->password =Hash::make($input['password']);
         $user->profile->about=$request->about;
         $user->save();
         $user->profile->save();

    return $this->sendResponse($user->toArray(),'avatar profile uploaded succesfully');
    }
}