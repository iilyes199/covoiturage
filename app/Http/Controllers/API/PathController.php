<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Path;
use App\User;
use App\Pathdetail;
use App\Location;
use App\Http\Controllers\API;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class PathController extends BaseController
{



  public function pathdetails($id){


        
    $details[] = Path::where('id','=',$id)->select('departure_delegation',
    'arrival_delegation','cartype','preference')->first();
     
    $user = Path::find($id)->user;

    return $this->sendResponse2($details,$user,'Get path details Seccessfully');
}

/////get path-location-user of information passed on parameter
  public function driverpath($govad,$govar,$datdep){
   
    $path=Path::where([['departure_governorate','=',$govad],
   ['arrival_governorate','=',$govar],['departure_date','>', $datdep],['user_id','<>',Auth::user()->id]])->get();

 
   foreach($path as $p)
  $location[]=Location::where('user_id','=',$p->user_id)->latest()->first();
    
   foreach($path as $pp)
   $user[]=User::where([['id','=',$pp->user_id],['id','<>',Auth::user()->id]])->first();

   //,['id','<>',Auth::user()->id]
        
   return $this->sendr($path,$user,$location,'seccessfully');
}


//get paths with information passed on parameter
public function getpaths($govad,$govar,$datdep){
  
  $path=Path::where([['departure_governorate','=',$govad],
  ['arrival_governorate','=',$govar],['departure_date','>', $datdep]])->get();

  foreach($path as $pp)
  $username[]=User::where('id','=',$pp->user_id)->select('name')->get();
  return $this->sendResponse2($path,$username,'seccessfully');

}

public function getdetailedpaths($dd,$da,$dated,$gn){

  
  $path=Path::where([['departure_date','>',$dated],['departure_delegation','=',$dd],['arrival_delegation','=',$da]])->get();
   
  foreach($path as $p)
  $user[]=User::where([['id','=',$p->user_id],['gender','=',$gn]])->get();
 


  return $this->sendResponse2($path,$user,'success');
}



////create path
 public function create(Request $request){
    $input = $request->all();
    $current_time=\Carbon\Carbon::now();
    $validator = Validator::make($input,[
 
'departure_governorate' => 'required',
'arrival_governorate' => 'required',

'departure_date' => 'required|date|after:now',
'numplaces' => 'required|numeric|gt:0|gt:numplacesavailable',
'numplacesavailable' => 'required|numeric|gt:0',
'price' => 'required|gt:0', 
            
    ]);
    if ($validator -> fails()) {
        return $this->sendError('error validation',$validator->errors());
    }

      $path = Path::create([
          'user_id' =>auth()->id(),
        'departure_governorate' => request('departure_governorate'),
        'arrival_governorate' => request('arrival_governorate'),
        'departure_delegation' => request('departure_delegation'),
        'arrival_delegation' => request('arrival_delegation'),
        'departure_date' => Carbon::parse(request('departure_date'))->format('yy-m-d H:i'),        
        'numplaces' => request('numplaces'),
        'numplacesavailable' => request('numplacesavailable'),
        'price' => request('price'),
        'cartype' => request('cartype'),
        'preference' => request('preference')   
      ]);
    



      return $this->sendResponse($path,'Path created succesfully');
         
           


  }






}