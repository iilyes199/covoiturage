<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delegation extends Model
{
        /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
    'governorate_id','gouvname','name'
    
];
}
