<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'user_id','lat', 'lng', 'city'
    ];


    public function user() {

      return $this->belongsTo('App\User');

  }


}
