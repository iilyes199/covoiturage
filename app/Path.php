<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Reservation;
use App\Pathdetail;
class Path extends Model
{


  
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
       'user_id', 'departure_governorate', 'arrival_governorate','departure_date',
       'arrival_date','numplaces','numplacesavailable','price',
       'departure_delegation','arrival_delegation','cartype','preference'
   ];

   public function pathdetail() {

    return $this->hasOne('App\Pathdetail');

}

public function user() {

    return $this->belongsTo('App\User');

}
public function reservation() {

    return $this->hasMany('App\Reservation');

}


}
